#ifndef LZ77_H
 #define LZ77_H

#include <stdint.h>
#include <unistd.h>

// Returns the number of compressed bytes, or -1 on error
ssize_t lz77_compress(const uint8_t *corpus, size_t sz,
		uint8_t **result);


#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lz77.h"

int main(int argc, char *argv[])
{
	if(argc != 2) {
		fprintf(stderr, "Usage: %s <string>\n", argv[0]);
		return 1;
	}

	uint8_t *compressed;

	ssize_t bytecount = lz77_compress((unsigned char *)argv[1], strlen(argv[1]), &compressed);


	fwrite(compressed, 1, bytecount, stdout);

	free(compressed);
}

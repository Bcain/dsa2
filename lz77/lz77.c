#include "lz77.h"

#include <stdlib.h>
#include <stdio.h>

enum { WINDOW_SIZE=32 };

struct triplet {
	uint8_t offset;
	uint8_t length;
	uint8_t end;
	struct triplet *next;
};

void ll_print(struct triplet *ll)
{
	while(ll) {
		printf("(%hhu, %hhu, %c) →", ll->offset, ll->length, ll->end);
		ll = ll->next;
	}
	puts("");
}

void ll_destroy(struct triplet *ll)
{
	while(ll) {
		struct triplet *tmp = ll->next;
		free(ll);
		ll = tmp;
	}
}

ssize_t lz77_compress(const uint8_t *corpus, size_t sz,
		uint8_t **result)
{
	if(!corpus || sz == 0) {
		*result = NULL;
		return -1;
	}

	size_t idx=0;

	struct triplet *token = malloc(sizeof(*token));
	if(!token) {
		*result = NULL;
		return -1;
	}
	token->offset = 0;
	token->length = 0;
	token->end = corpus[0];
	token->next = NULL;
	struct triplet *head = token;
	++idx;
	size_t token_count=1;

	size_t window_start=0, window_limit=1;

	while(idx < sz) {
		size_t match_limit=0, match_start=0;
		for(size_t n=window_start; n < window_limit; ++n) {
			if(corpus[n] == corpus[idx]) {
				size_t match_length=1;
				// While the window is matching the corpus to encode,
				// AND the submatch does not escape the bounds of either
				// the window or the corpus
				while(n+match_length < window_limit &&
						idx+match_length < sz-1 &&
						corpus[n+match_length] == corpus[idx+match_length]) {
					++match_length;
				}

				if(match_length > match_limit - match_start) {
					match_start = n;
					match_limit = match_start + match_length;
				}
			}
		}

		token->next = malloc(sizeof(*token->next));
		if(!token) {
			ll_destroy(head);
			*result = NULL;
			return -1;
		}
		token = token->next;
		token->length = match_limit - match_start;
		token->offset = match_start - window_start;
		if(token->length > 0 && WINDOW_SIZE > window_limit) {
			token->offset += WINDOW_SIZE - window_limit;
		}
		token->end = corpus[idx + token->length];
		token->next = NULL;
		++token_count;

		idx += token->length + 1;
		window_limit += token->length + 1;
		if(window_limit > WINDOW_SIZE) {
			window_start = window_limit - WINDOW_SIZE;
		}
	}

	// The times-three comes from the encoding scheme of offset, length, end
	ssize_t payload_sz = token_count * 3;

	uint8_t *payload = malloc(payload_sz);
	if(!payload) {
		ll_destroy(head);
		*result = NULL;
		return -1;
	}

	*result = payload;
	token = head;
	while(token) {
		*payload++ = token->offset;
		*payload++ = token->length;
		*payload++ = token->end;
		token = token->next;
	}

	ll_destroy(head);

	return payload_sz;
}
